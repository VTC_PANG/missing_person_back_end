const route = require('express').Router()
const {validate, ValidationError} = require('express-json-validator')
const missing_personSchema = require('../models/missing_person').missing_personSchema
const Person = require('../controllers/missing_person')
const User = require('../controllers/user')
var jwt = require('jsonwebtoken');
var config = require('../config');

const Handler_Person = new Person()

route.post('/users', async (req, res) => {
    // Create a new user
    try {
        const user = new User(req.body)
        await user.save()
        console.log(`_id: ${user._id}`)
        console.log(`TOKEN_SECRET: ${process.env.JWT_KEY}`)
        const token = jwt.sign({_id: user._id}, process.env.JWT_KEY)
        console.log(`token: ${token}`)
        user.tokens = user.tokens.concat({token})
        await user.save()


        res.status(201).send({ user, token })
    } catch (error) {
      console.log(`Fail: ${error}`)
        res.status(400).send(error)
  
    }
  })
  
Handler_Person.collectionConnect().then( () => {
    route.get('/test', (req, res) => { Handler_Person.test(req, res) })

    route.get('/allPerson', (req, res) => { Handler_Person.allPerson(req, res)})
    route.get('/getPersonByName', (req, res) => { Handler_Person.getPersonByName (req,res)})

    route.post('/newPerson', validate(missing_personSchema), (req, res) => { Handler_Person.newPerson (req,res)})

    route.put('/updatePerson',  validate(missing_personSchema), (req, res) => { Handler_Person.updatePerson (req,res)})

    route.delete('/deletePerson', validate(missing_personSchema), (req, res) => { Handler_Person.deletePerson(req, res)})

    route.use((err, req, res, next)=>{
        if(err) {
            res.status(422).send({"status": 422, "description": err.message})            
        } else { next() }
    })
})
.catch(
    err=> console.error(`Error: ${err.message}`)
)



module.exports = route

