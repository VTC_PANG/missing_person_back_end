const MongoClient = require("mongodb").MongoClient

const config = require('../config');

const uri = "mongodb+srv://admin_paul:a123456@cluster0.pmhht.mongodb.net/missing_persons?retryWrites=true&w=majority";

const DATABASE_NAME = "missing_persons"

var database, collection
//var result=[]

module.exports = class Person {
    collectionConnect() { 
        return new Promise( (resolve, reject) => {
            MongoClient.connect(uri, { useUnifiedTopology: true , useNewUrlParser: true }, (error, client) => {
                if(error) {
                    reject(new Error(error))
                }
                database = client.db(DATABASE_NAME)
                collection = database.collection("person")
            
            }) 
            resolve(console.log("Connected to `" + DATABASE_NAME + "`!"))
        })
    }

    test(req, res) {
        res.send({"status": 200, "message": "Test person"})
    }

    allPerson(req, res) {
        console.log(`Someone request all person`)
        collection.find().toArray((err, docs) => {
            if(err) {
                res.status(500).send({"status":500, "description":err})
            } 
            const mPerson = docs
            const result = {mPerson}
            // res.send(result)
            res.status(200).send({"status": 200, "data": result})
        })
    }

    getPersonByName(request, response) {
        console.log(`Someone make query ${request.query.name}`)
        collection.find({$or: [{"mname": {$regex: request.query.name}}, {"emname": {$regex: request.query.name}}]}).toArray((err, docs) =>{
            if(err) {
                response.status(500).send({"status":500, "description":err})
            } 
            const mPerson = docs
            const result = {mPerson}
            // res.send(result)
            response.status(200).send({"status": 200, "data": result})
        })
    }

    newPerson(request, response) {
        collection.insertOne(request.body, (err) => {
            if(err){
                response.status(500).send({"status": 500, "description" : err})
            }
            response.status(201).send({"status": 200, "description": "Data input successfully"})   // Created
        })
    }    


    updatePerson(request, response) {
        console.log(`Someone update ${request.query.mname}`)
        collection.findOneAndUpdate({mname: request.body.mname},
            {$set: request.body}, 
            {}, 
            (err) => {
               if(err) {
                   response.status(500).send({"status": 500, "description": err})
               }
               response.status(200).send({"status": 200, "description": "Data update successfully"}) 
           }
    
       )
    }

    deletePerson(request, response) {
		console.log(`Someone delete ${request.query.name}`)
        collection.findOneAndDelete({$or: [{"mname": {$regex: request.query.name}}, {"emname": {$regex: request.query.name}}]},
             
             (err) => {
                if(err) {
                    response.status(500).send({"status": 500, "description": err})
                }
                response.status(200).send({"status": 200, "description": "Data deleted successfully"})
            }
        )
    }
}
