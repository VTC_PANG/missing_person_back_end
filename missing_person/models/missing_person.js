// Create a schema
exports.missing_personSchema =
{
    type: 'object',
    required: ["mname","emname", "face", "mdate", "wear", "age", "gender", "last_location"],
    properties: {
        case_id: { type: "string" },
        status: { type: "string" },
        mname: { type: "string" },
        emname: { type: "string" },
        name2: { type: "string" },
        face: { type: "string" },
        mdate: { type: "string" },
        wear: { type: "string" },
        other: { type: "string" },
        other2: { type: "string" },
        img: { type: "string" },
        age: { type: "string" },
        gender: { type: "string" },
        last_location: { type: "string" },
        last_location_lat: { type: "string" },
        last_location_lng: { type: "string" }
    }
}