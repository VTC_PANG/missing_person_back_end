var cors = require("cors");
const Express = require('express')
const BodyParser = require('body-parser')
const Router = require('./routes/router')
const App = Express()
var config = require('./config');

var mongoose = require("mongoose");

mongoose.connect(config.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
})



App.use(cors())
App.use(BodyParser.json())
App.use(BodyParser.urlencoded({ extended: true }))
// App.use(userRouter)
App.use('/', Router)


App.use(function(req, res, next) {
    var protocol = req.get('x-forwarded-proto');
    protocol == 'https' ? next() : res.redirect('https://' + req.hostname + req.url);
});

  // 5. Connect to MongoDB
mongoose.connect(config.MONGO_URI, { useUnifiedTopology: true , useNewUrlParser: true });
mongoose.connection.on('error', function(err) {
  console.log('Error: Could not connect to MongoDB.');
});

const port = 10900

App.listen(config.LISTEN_PORT, () => {
    console.log(`Server is up and running on port number ${port}`)
})

